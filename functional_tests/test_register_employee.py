from accounts.models import User
from .base import CustomLiveTestCase

REGISTER_URL = '/en/employee/register/'
REGISTER_BTN_XPATH = '/html/body/div/div/div/form/div[7]/button'
BOX_XPATH = '/html/body/div/div/div'


class RegisterEmployeeTest(CustomLiveTestCase):
    def setUp(self):
        super().setUp()

        self.employee = {
            "first_name": "Muhammad",
            "last_name": "Yusuf",
            "email": "muhammad_yusuf@gmail.com",
            "role": "employee",
            "password": "123456",
        }
        self.user = User.objects.create(**self.employee)

    def test_register_without_fill_any(self):
        self.browser.get(self.live_server_url + REGISTER_URL)
        self.browser.find_element_by_xpath(REGISTER_BTN_XPATH).click()
        self.assertIn('This field is required',
                      self.browser.find_element_by_xpath(BOX_XPATH).text)

    def test_register_without_gender_chosen(self):
        self.browser.get(self.live_server_url + REGISTER_URL)
        self.browser.find_element_by_name('first_name').send_keys('Lucinta')
        self.browser.find_element_by_name('last_name').send_keys('Luna')
        self.browser.find_element_by_name('email').send_keys(
            'lucintaluna@pmpl.com')
        self.browser.find_element_by_name('password1').send_keys('ukhakh12')
        self.browser.find_element_by_name('password2').send_keys('ukhakh12')
        self.browser.find_element_by_xpath(REGISTER_BTN_XPATH).click()
        self.assertIn('Gender is required',
                      self.browser.find_element_by_xpath(BOX_XPATH).text)

    def test_register_with_unmatch_password(self):
        self.browser.get(self.live_server_url + REGISTER_URL)
        self.browser.find_element_by_name('first_name').send_keys('Kebiasaan')
        self.browser.find_element_by_name('last_name').send_keys('Ku')
        self.browser.find_element_by_name('email').send_keys(
            'kebiasaanku@pmpl.com')
        self.browser.find_element_by_name('password1').send_keys('lIlIIIIlli')
        self.browser.find_element_by_name('password2').send_keys('lIlIlIIlli')
        self.browser.find_element_by_id('male gender').click()
        self.browser.find_element_by_xpath(REGISTER_BTN_XPATH).click()
        self.assertIn('The two password fields didn’t match',
                      self.browser.find_element_by_xpath(BOX_XPATH).text)

    def test_register_success(self):
        self.browser.get(self.live_server_url + REGISTER_URL)
        self.browser.find_element_by_name('first_name').send_keys('Surya')
        self.browser.find_element_by_name('last_name').send_keys('Alif')
        self.browser.find_element_by_name('email').send_keys(
            'surya_alif@pmpl.com')
        self.browser.find_element_by_name('password1').send_keys('h4rUzSu54H')
        self.browser.find_element_by_name('password2').send_keys('h4rUzSu54H')
        self.browser.find_element_by_id('male gender').click()
        self.browser.find_element_by_xpath(REGISTER_BTN_XPATH).click()
        self.assertIn('Login',
                      self.browser.find_element_by_xpath(BOX_XPATH).text)

    def test_register_with_registered_email(self):
        self.browser.get(self.live_server_url + REGISTER_URL)
        self.browser.find_element_by_name('first_name').send_keys('Yusuf')
        self.browser.find_element_by_name('last_name').send_keys('Muhammad')
        self.browser.find_element_by_name('email').send_keys(
            'muhammad_yusuf@gmail.com')
        self.browser.find_element_by_name('password1').send_keys(
            'aKuPuny4KemB4ran')
        self.browser.find_element_by_name('password2').send_keys(
            'aKuPuny4KemB4ran')
        self.browser.find_element_by_id('male gender').click()
        self.browser.find_element_by_xpath(REGISTER_BTN_XPATH).click()
        self.assertIn('A user with that email already exists',
                      self.browser.find_element_by_xpath(BOX_XPATH).text)
