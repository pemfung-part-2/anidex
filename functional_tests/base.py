from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options


class CustomLiveTestCase(StaticLiveServerTestCase):
    def setUp(self):
        super().setUp()

        chrome_options = Options()
        chrome_options.add_argument("--disable-application-cache")
        chrome_options.add_argument("--dns-prefetch-disable")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")

        self.browser = Chrome(
            executable_path="./chromedriver",
            options=chrome_options,
        )

    def tearDown(self):
        self.browser.quit()
        super().tearDown()
