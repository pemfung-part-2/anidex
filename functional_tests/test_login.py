from django.contrib.auth.hashers import make_password

from accounts.models import User
from .base import CustomLiveTestCase

LOGIN_URL = '/en/login/'
LOGIN_BTN_XPATH = '/html/body/div/div/div/form/div[3]/button'
BODY_XPATH = '/html/body'


class LoginTest(CustomLiveTestCase):
    def setUp(self):
        super().setUp()

        self.employer = {
            "first_name": "sejuk",
            "last_name": "selamanya",
            "email": "contact@sejuk-selamanya.com",
            "role": "employer",
            "password": make_password("123456"),
        }
        self.user = User.objects.create(**self.employer)

        self.employee = {
            "first_name": "Muhammad",
            "last_name": "Yusuf",
            "email": "muhammad_yusuf@gmail.com",
            "role": "employee",
            "password": make_password("123456"),
        }
        self.user2 = User.objects.create(**self.employee)

    def test_employee_login_success(self):
        self.browser.get(self.live_server_url + LOGIN_URL)
        self.browser.find_element_by_name('email').send_keys(
            'muhammad_yusuf@gmail.com')
        self.browser.find_element_by_name('password').send_keys('123456')
        self.browser.find_element_by_xpath(LOGIN_BTN_XPATH).click()
        self.assertIn('Find a job you will love.',
                      self.browser.find_element_by_xpath(BODY_XPATH).text)
        self.assertIn('Edit Profile',
                      self.browser.find_element_by_xpath(BODY_XPATH).text)

    def test_employer_login_success(self):
        self.browser.get(self.live_server_url + LOGIN_URL)
        self.browser.find_element_by_name('email').send_keys(
            'contact@sejuk-selamanya.com')
        self.browser.find_element_by_name('password').send_keys('123456')
        self.browser.find_element_by_xpath(LOGIN_BTN_XPATH).click()
        self.assertIn('Find a job you will love.',
                      self.browser.find_element_by_xpath(BODY_XPATH).text)
        self.assertIn('Dashboard',
                      self.browser.find_element_by_xpath(BODY_XPATH).text)

    def test_invalid_credentials(self):
        self.browser.get(self.live_server_url + LOGIN_URL)
        self.browser.find_element_by_name('email').send_keys('admin@coba.com')
        self.browser.find_element_by_name('password').send_keys('qwerty')
        self.browser.find_element_by_xpath(LOGIN_BTN_XPATH).click()
        self.assertIn('User Does Not Exist.',
                      self.browser.find_element_by_xpath(BODY_XPATH).text)
