from accounts.models import User
from .base import CustomLiveTestCase

REGISTER_URL = '/en/employer/register/'
REGISTER_BTN_XPATH = '/html/body/div/div/div/form/div[6]/button'
BOX_XPATH = '/html/body/div/div/div'


class RegisterEmployerTest(CustomLiveTestCase):
    def setUp(self):
        super().setUp()

        self.employer = {
            "first_name": "sejuk",
            "last_name": "selamanya",
            "email": "contact@sejuk-selamanya.com",
            "role": "employer",
            "password": "123456",
        }
        self.user = User.objects.create(**self.employer)

    def test_register_without_fill_any(self):
        self.browser.get(self.live_server_url + REGISTER_URL)
        self.browser.find_element_by_xpath(REGISTER_BTN_XPATH).click()
        self.assertIn('This field is required',
                      self.browser.find_element_by_xpath(BOX_XPATH).text)

    def test_register_with_unmatch_password(self):
        self.browser.get(self.live_server_url + REGISTER_URL)
        self.browser.find_element_by_name('first_name').send_keys('dirgahayu')
        self.browser.find_element_by_name('last_name').send_keys('tokoku')
        self.browser.find_element_by_name('email').send_keys(
            'dirgahayu@pmpl.com')
        self.browser.find_element_by_name('password1').send_keys('jeruknipis')
        self.browser.find_element_by_name('password2').send_keys('jerukbali')
        self.browser.find_element_by_xpath(REGISTER_BTN_XPATH).click()
        self.assertIn('The two password fields didn’t match',
                      self.browser.find_element_by_xpath(BOX_XPATH).text)

    def test_register_success(self):
        self.browser.get(self.live_server_url + REGISTER_URL)
        self.browser.find_element_by_name('first_name').send_keys('Gudang')
        self.browser.find_element_by_name('last_name').send_keys('Gula')
        self.browser.find_element_by_name('email').send_keys(
            'mail@gudanggula.com')
        self.browser.find_element_by_name('password1').send_keys('hah4hih1')
        self.browser.find_element_by_name('password2').send_keys('hah4hih1')
        self.browser.find_element_by_xpath(REGISTER_BTN_XPATH).click()
        self.assertIn('Login',
                      self.browser.find_element_by_xpath(BOX_XPATH).text)

    def test_register_with_registered_email(self):
        self.browser.get(self.live_server_url + REGISTER_URL)
        self.browser.find_element_by_name('first_name').send_keys('selamanya')
        self.browser.find_element_by_name('last_name').send_keys('sejuk')
        self.browser.find_element_by_name('email').send_keys(
            'contact@sejuk-selamanya.com')
        self.browser.find_element_by_name('password1').send_keys('suksesamiin')
        self.browser.find_element_by_name('password2').send_keys('suksesamiin')
        self.browser.find_element_by_xpath(REGISTER_BTN_XPATH).click()
        self.assertIn('A user with that email already exists',
                      self.browser.find_element_by_xpath(BOX_XPATH).text)
