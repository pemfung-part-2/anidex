from django import template

from jobsapp.models import Applicant

register = template.Library()


@register.simple_tag(name="is_already_applied")
def is_already_applied(job, user):
    return Applicant.objects.filter(job=job, user=user)
