from django import template

register = template.Library()


@register.simple_tag(name="tag_exists")
def tag_exists(tag_id, tags):
    return tags is not None and len(tags) > 0 and str(tag_id) in tags


@register.filter
def get_item(dictionary, key):
    return dict(dictionary).get(key)
