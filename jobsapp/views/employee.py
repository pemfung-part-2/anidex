from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView

from jobsapp.decorators import user_is_employee
from jobsapp.models import Favorite, Applicant


@method_decorator(login_required(login_url=reverse_lazy("accounts:login")),
                  name="dispatch")
@method_decorator(user_is_employee, name="dispatch")
class EmployeeMyJobsListView(ListView):
    model = Applicant
    template_name = "jobs/employee/my-applications.html"
    context_object_name = "applicants"
    paginate_by = 6

    def get_queryset(self):
        self.queryset = (self.model.objects.select_related("job").filter(
            user_id=self.request.user.id).order_by("-created_at"))
        if ("status" in self.request.GET
                and len(self.request.GET.get("status")) > 0
                and int(self.request.GET.get("status")) > 0):
            self.queryset = self.queryset.filter(
                status=int(self.request.GET.get("status")))
        return self.queryset

@method_decorator(login_required(login_url=reverse_lazy("accounts:login")),
                  name="dispatch")
@method_decorator(user_is_employee, name="dispatch")
class FavoriteListView(ListView):
    model = Favorite
    template_name = "jobs/employee/favorites.html"
    context_object_name = "favorites"

    def get_queryset(self):
        return self.model.objects.select_related("job__user").filter(
            job__filled=False, soft_deleted=False, user=self.request.user)
