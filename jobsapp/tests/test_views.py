from datetime import datetime, timedelta

from django.test import TestCase
from django.urls import reverse
from django.utils import translation

from jobsapp.models import Applicant, Job, User, Favorite

SAMPLE_EMAIL = "test1@test2.com"
SAMPLE_EMAIL2 = "test3@test4.com"
SAMPLE_PASSWORD = "ijklmno.1"
ROUTE_APPLY_JOB = "jobs:apply-job"
ROUTE_FAV = "jobs:favorite"

class BaseTest(TestCase):
    def setUp(self) -> None:
        self.language_code = translation.get_language()
        self.user = User.objects.create_user(password=SAMPLE_PASSWORD,
                                             email=SAMPLE_EMAIL,
                                             role="employee")
        self.user2 = User.objects.create_user(password=SAMPLE_PASSWORD,
                                             email=SAMPLE_EMAIL2,
                                             role="employer")

    @classmethod
    def setUpTestData(cls) -> None:
        cls.valid_job = {
            "title": "Junior Software Engineer",
            "description": "Looking for Python developer",
            "salary": 35000,
            "location": "Dhaka, Bangladesh",
            "type": "1",
            "category": "web-development",
            "last_date": datetime.now() + timedelta(days=30),
            "company_name": "Dev Soft",
            "company_description": "A foreign country",
            "website": "www.devsoft.com",
        }
        cls.employer = {
            "first_name": "John",
            "last_name": "Doe",
            "email": "employer@gmail.com",
            "role": "employer",
            "password": "123456",
        }
        cls.user = User.objects.create(**cls.employer)
        cls.job = Job(**cls.valid_job)
        cls.job.user = cls.user
        cls.job.save()
        cls.applicant = Applicant.objects.create(user=cls.user, job=cls.job)
        cls.favorite = Favorite.objects.create(user=cls.user, job=cls.job)

class TestHomeView(TestCase):
    def test_context(self):
        response = self.client.get(reverse("jobs:home"))
        self.assertGreaterEqual(len(response.context["jobs"]), 0)

    def test_template_used(self):
        response = self.client.get(reverse("jobs:home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "home.html")


class TestSearchView(TestCase):
    def setUp(self):
        self.url = reverse("jobs:search")
        super().setUp()

    def test_empty_query(self):
        jobs = Job.objects.filter(title__contains="software")
        response = self.client.get(self.url + "?position=software")
        self.assertFalse(b"We have found %a jobs" %
                         str(jobs.count()) in response.content.lower())


class TestJobDetailsView(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestJobDetailsView, cls).setUpTestData()
        cls.valid_job_id = cls.job.id

    def test_details(self):
        response = self.client.get(reverse("jobs:jobs-detail", args=(1, )))
        self.assertEqual(response.status_code, 404)

    def test_valid_details(self):
        response = self.client.get(reverse("jobs:jobs-detail", args=(self.valid_job_id, )))
        self.assertEqual(response.status_code, 200)

class TestApplyJobView(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestApplyJobView, cls).setUpTestData()
        cls.valid_job_id = cls.job.id
        cls.valid_user_id = cls.user.id

    def setUp(self) -> None:
        super().setUp()
        self.client.login(email=SAMPLE_EMAIL, password=SAMPLE_PASSWORD)

    def test_apply_job(self):
        response = self.client.post(
            reverse(ROUTE_APPLY_JOB, args=(self.valid_job_id, )),
            {
                "applicant": {
                    "user": {
                        "id": self.valid_user_id
                    }
                },
                "job": self.valid_job_id
            },
        )
        self.assertEqual(response.status_code, 302)

    def test_failed_apply_job(self):
        response = self.client.post(
            reverse(ROUTE_APPLY_JOB, args=(223, )),
        )
        self.assertEqual(response.status_code, 302)

    def test_apply_job_twice(self):
        self.client.post(
            reverse(ROUTE_APPLY_JOB, args=(self.valid_job_id, )),
            {
                "applicant": {
                    "user": {
                        "id": self.valid_user_id
                    }
                },
                "job": self.valid_job_id
            },
        )
        response = self.client.post(
            reverse(ROUTE_APPLY_JOB, args=(self.valid_job_id, )),
            {
                "applicant": {
                    "user": {
                        "id": self.valid_user_id
                    }
                },
                "job": self.valid_job_id
            },
        )
        self.assertEqual(response.status_code, 302)

class TestFavoriteView(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestFavoriteView, cls).setUpTestData()
        cls.valid_job_id = cls.job.id
        cls.valid_user_id = cls.user.id

    def setUp(self) -> None:
        super().setUp()
        self.client.login(email=SAMPLE_EMAIL, password=SAMPLE_PASSWORD)

    def test_favorite_job(self):
        response = self.client.post(
            reverse(ROUTE_FAV), {
                "job_id": self.valid_job_id
            }
        )
        self.assertEqual(response.status_code, 200)

    def test_unfavorite_job(self):
        response = self.client.post(
            reverse(ROUTE_FAV), {
                "job_id": self.valid_job_id
            }
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.post(
            reverse(ROUTE_FAV), {
                "job_id": self.valid_job_id
            }
        )
        self.assertEqual(response.status_code, 200)

    def test_favorite_fail_not_authenticated(self):
        self.client.logout()
        response = self.client.post(
            reverse(ROUTE_FAV), {
                "job_id": self.valid_job_id
            }
        )
        self.assertEqual(response.status_code, 200)

class TestEmployeeMyJobsListView(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestEmployeeMyJobsListView, cls).setUpTestData()

    def setUp(self) -> None:
        super().setUp()
        self.url = reverse("jobs:employee-my-applications")
        self.client.login(email=SAMPLE_EMAIL, password=SAMPLE_PASSWORD)

    def test_my_joblist(self):
        response = self.client.get(self.url + "?status=2")
        self.assertEqual(response.status_code, 200)

class TestFavoriteListView(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestFavoriteListView, cls).setUpTestData()

    def setUp(self) -> None:
        super().setUp()
        self.url = reverse("jobs:employee-favorites")
        self.client.login(email=SAMPLE_EMAIL, password=SAMPLE_PASSWORD)

    def test_get_favorite_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

class TestDashboardView(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestDashboardView, cls).setUpTestData()
        cls.valid_job_id = cls.job.id

    def setUp(self) -> None:
        super().setUp()
        self.url = reverse("jobs:employer-dashboard")
        self.client.login(email=SAMPLE_EMAIL2, password=SAMPLE_PASSWORD)

    def test_get_favorite_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

class TestApplicantPerJobView(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestApplicantPerJobView, cls).setUpTestData()
        cls.valid_job_id = cls.job.id
        cls.valid_user_id = cls.user.id

    def setUp(self) -> None:
        super().setUp()
        self.client.login(email=SAMPLE_EMAIL2, password=SAMPLE_PASSWORD)

    def test_get_favorite_list(self):
        url = reverse("jobs:employer-dashboard-applicants", args=(self.valid_job_id, ))
        response = self.client.get(url,
            {
                "applicant": {
                    "user": {
                        "id": self.valid_user_id
                    }
                },
                "job": self.valid_job_id
            }
        )
        self.assertEqual(response.status_code, 200)

class TestApplicantsListView(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestApplicantsListView, cls).setUpTestData()

    def setUp(self) -> None:
        super().setUp()
        self.url = reverse("jobs:employer-all-applicants")
        self.client.login(email=SAMPLE_EMAIL2, password=SAMPLE_PASSWORD)

    def test_get_applicants_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

class TestFilled(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestFilled, cls).setUpTestData()
        cls.valid_job_id = cls.job.id

    def setUp(self) -> None:
        super().setUp()
        self.client.login(email=SAMPLE_EMAIL2, password=SAMPLE_PASSWORD)

    def test_get_filled_list(self):
        self.job.user = self.user2
        self.job.save()
        url = reverse("jobs:job-mark-filled", args=(self.valid_job_id, ))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
