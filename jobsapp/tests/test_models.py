from datetime import datetime, timedelta

from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import translation

from accounts.models import User
from jobsapp.models import Job, Applicant, Favorite


class BaseTest(TestCase):
    def setUp(self) -> None:
        self.language_code = translation.get_language()

    @classmethod
    def setUpTestData(cls) -> None:
        cls.valid_job = {
            "title": "Junior Software Engineer",
            "description": "Looking for Python developer",
            "salary": 35000,
            "location": "Dhaka, Bangladesh",
            "type": "1",
            "category": "web-development",
            "last_date": datetime.now() + timedelta(days=30),
            "company_name": "Dev Soft",
            "company_description": "A foreign country",
            "website": "www.devsoft.com",
        }
        cls.employer = {
            "first_name": "John",
            "last_name": "Doe",
            "email": "employer@gmail.com",
            "role": "employer",
            "password": "123456",
        }
        cls.employer2 = {
            "first_name": "Jown",
            "last_name": "Dou",
            "email": "employer2@gmail.com",
            "role": "employer",
            "password": "123456",
        }
        cls.employer3 = {
            "first_name": "Kohn",
            "last_name": "Toe",
            "email": "employer3@gmail.com",
            "role": "employer",
            "password": "123456",
        }
        cls.user = User.objects.create(**cls.employer)
        cls.user2 = User.objects.create(**cls.employer2)
        cls.user3 = User.objects.create(**cls.employer3)
        cls.job = Job(**cls.valid_job)
        cls.job.user = cls.user
        cls.job.save()


class TestJobModel(BaseTest):
    def test_get_absolute_url(self):
        self.assertURLEqual(self.job.get_absolute_url(),
                            f"/{self.language_code}/jobs/" + str(self.job.id) + '/')

    def test_title_max_length(self):
        max_length = self.job._meta.get_field("title").max_length
        self.assertEqual(max_length, 300)

    def test_location_max_length(self):
        max_length = self.job._meta.get_field("location").max_length
        self.assertEqual(max_length, 150)

    def test_category_max_length(self):
        max_length = self.job._meta.get_field("category").max_length
        self.assertEqual(max_length, 100)

    def test_company_name_max_length(self):
        max_length = self.job._meta.get_field("company_name").max_length
        self.assertEqual(max_length, 100)

    def test_company_description_max_length(self):
        max_length = self.job._meta.get_field("company_description").max_length
        self.assertEqual(max_length, 300)

    def test_website_max_length(self):
        max_length = self.job._meta.get_field("website").max_length
        self.assertEqual(max_length, 100)

    def test_invalid_type_choices(self):
        invalid_type = {
            "title": "Junior Software Engineer",
            "description": "Looking for Python developer",
            "salary": 35000,
            "location": "Dhaka, Bangladesh",
            "type": "100",
            "category": "web-development",
            "last_date": datetime.now() + timedelta(days=30),
            "company_name": "Dev Soft",
            "company_description": "A foreign country",
            "website": "www.devsoft.com",
        }
        job = Job(**invalid_type)
        job.user = self.user
        with self.assertRaises(ValidationError):
            job.full_clean()

    def test_create_job_without_user(self):
        job = Job(**self.valid_job)
        with self.assertRaises(ValidationError):
            job.full_clean()

    def test_title_label(self):
        field_label = self.job._meta.get_field("title").verbose_name
        self.assertEqual(field_label, "title")

    def test_string_representation(self):
        self.assertEqual(str(self.job), self.job.title)

class TestApplicantModel(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestApplicantModel, cls).setUpTestData()
        cls.applicant = Applicant.objects.create(user=cls.user, job=cls.job)
        cls.applicant_accepted = Applicant.objects.create(user=cls.user2, job=cls.job, status=2)
        cls.applicant_rejected = Applicant.objects.create(user=cls.user3, job=cls.job, status=3)

    def test_get_status_pending(self):
        self.assertEqual(self.applicant.get_status, "Pending")
        self.assertEqual(self.applicant_accepted.get_status, "Accepted")
        self.assertEqual(self.applicant_rejected.get_status, "Rejected")

    def test_str(self):
        self.assertEqual(self.applicant.__str__(), self.user.get_full_name())

class TestFavoriteModel(BaseTest):
    @classmethod
    def setUpTestData(cls) -> None:
        super(TestFavoriteModel, cls).setUpTestData()
        cls.favorite = Favorite.objects.create(user=cls.user, job=cls.job)

    def test_string_representation(self):
        self.assertEqual(str(self.favorite), self.favorite.job.title)
