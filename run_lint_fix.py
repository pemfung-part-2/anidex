#!/usr/bin/env python
import os
import subprocess

# Check for directories
EXCLUDE = [
    ".env",
    ".env.dev.sample",
    ".git",
    ".gitattributes",
    ".gitignore",
    ".vscode",
    ".idea",
    "coverage",
    "static",
    "env",
    ".github",
    "deployment",
    "locale/bn/LC_MESSAGES",
    "logs",
    "screenshots",
    "Jenkinsfile",
    "LICENSE",
    "Procfile",
    "Procfile.windows",
    "README.md",
    "pyproject.toml",
    "requirements.txt",
]
DIRS = [x for x in os.listdir() if x not in EXCLUDE and os.path.isdir(x)]
DIRS.append("manage.py")

subprocess.call(["yapf", "-r", "-i"] + DIRS)

