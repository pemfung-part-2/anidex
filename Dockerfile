FROM python:3.6-buster

WORKDIR /opt/django-job-portal
RUN mkdir -p /opt/django-job-portal

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /opt/django-job-portal

RUN pip install --upgrade pip \
    && pip install -r requirements.txt --no-cache-dir
    
COPY . /opt/django-job-portal

RUN chmod +x /opt/django-job-portal/docker_start.sh

EXPOSE 8000
