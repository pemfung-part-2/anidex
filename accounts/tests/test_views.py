from django.test import TestCase
from django.urls import reverse
from django.utils import translation

from accounts.models import User

SAMPLE_EMAIL = "test@test.com"
SAMPLE_PASSWORD = "Abcdefgh.1"
ROUTE_LOGIN = "accounts:login"


class BaseTest(TestCase):
    def setUp(self) -> None:
        self.language_code = translation.get_language()
        self.user = User.objects.create_user(password=SAMPLE_PASSWORD,
                                             email=SAMPLE_EMAIL)


class TestLoginView(BaseTest):
    def setUp(self) -> None:
        super().setUp()
        self.response = self.client.get(reverse(ROUTE_LOGIN))

    def test_csrf(self):
        self.assertTemplateUsed(self.response, "accounts/login.html")
        self.assertContains(self.response, "csrfmiddlewaretoken")

    def test_redirect_if_authenticated(self):
        self.client.login(email=SAMPLE_EMAIL, password=SAMPLE_PASSWORD)
        response = self.client.get(reverse(ROUTE_LOGIN))
        self.assertURLEqual(reverse("jobs:home"),
                            "/" + self.language_code + response.url)
        self.client.logout()

    def test_submit_form(self):
        response = self.client.post(
            reverse(ROUTE_LOGIN),
            {
                "email": SAMPLE_EMAIL,
                "password": SAMPLE_PASSWORD
            },
        )
        self.assertURLEqual(reverse("jobs:home"),
                            "/" + self.language_code + response.url)


class TestLogoutView(BaseTest):
    def setUp(self) -> None:
        super().setUp()
        self.client.login(email=SAMPLE_EMAIL, password=SAMPLE_PASSWORD)

    def test_redirect_after_logout(self):
        response = self.client.get(reverse("accounts:logout"))
        self.assertEqual(response.status_code, 302)
