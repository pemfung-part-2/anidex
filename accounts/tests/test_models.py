from django.test import TestCase
from django.db.utils import DataError, IntegrityError

from accounts.models import User

SAMPLE_EMAIL = "rumi1@gmail.com"


class TestUserModel(TestCase):
    def setUp(self) -> None:
        self.valid_user = {
            "first_name": "Manjurul",
            "last_name": "Hoque",
            "role": "employee",
            "gender": "male",
            "email": SAMPLE_EMAIL,
            "password": "123456",
        }
        self.user = User.objects.create(**self.valid_user)

    def test_string_representation(self):
        self.assertEqual(str(self.user), self.user.email)

    def test_string_representatioin_not_equal(self):
        self.assertNotEqual(str(self.user), "Non email")

    def test_verbose_name_plural(self):
        self.assertEqual(str(User._meta.verbose_name_plural), "users")

    def test_full_name(self):
        self.assertEqual(self.user.get_full_name(), "Manjurul Hoque")

    def test_full_name_not_equal(self):
        self.assertNotEqual(self.user.get_full_name(), "Manjurul Hoques")

    def test_gender_length_more_than_10(self):
        invalid_gender = {
            "first_name": "Manjurul",
            "last_name": "Hoque",
            "role": "employee",
            "gender": "f" * 11,
            "email": SAMPLE_EMAIL,
            "password": "123456",
        }
        with self.assertRaises(DataError):
            User.objects.create(**invalid_gender)

    def test_role_length_more_than_12(self):
        invalid_role = {
            "first_name": "Manjurul",
            "last_name": "Hoque",
            "role": "A"*13,
            "gender": "male",
            "email": SAMPLE_EMAIL,
            "password": "123456",
        }
        with self.assertRaises(DataError):
            User.objects.create(**invalid_role)

    def test_email_not_unique(self):
        duplicate_email_user = {
            "first_name": "Manjurul",
            "last_name": "Hoque",
            "role": "employee",
            "gender": "male",
            "email": SAMPLE_EMAIL,
            "password": "123456",
        }
        with self.assertRaises(IntegrityError):
            User.objects.create(**duplicate_email_user)

    def test_email_label(self):
        user = User.objects.get(id=self.user.id)
        field_label = user._meta.get_field("email").verbose_name
        self.assertEqual(field_label, "email")
