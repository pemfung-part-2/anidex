from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm

from accounts.models import User

GENDER_CHOICES = (("male", "Male"), ("female", "Female"))

CONFIRM_PASSWORD_STR = "Confirm Password"
ENTER_EMAIL_STR = "Enter Email"
ENTER_PASSWORD_STR = "Enter Password"


class EmployeeRegistrationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["gender"].required = True
        self.fields["first_name"].label = "First Name"
        self.fields["last_name"].label = "Last Name"
        self.fields["password1"].label = "Password"
        self.fields["password2"].label = CONFIRM_PASSWORD_STR

        self.fields["first_name"].widget.attrs.update({
            "placeholder":
            "Enter First Name",
        })
        self.fields["last_name"].widget.attrs.update({
            "placeholder":
            "Enter Last Name",
        })
        self.fields["email"].widget.attrs.update({
            "placeholder": ENTER_EMAIL_STR,
        })
        self.fields["password1"].widget.attrs.update({
            "placeholder":
            ENTER_PASSWORD_STR,
        })
        self.fields["password2"].widget.attrs.update({
            "placeholder":
            CONFIRM_PASSWORD_STR,
        })

    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "email",
            "password1",
            "password2",
            "gender",
        ]
        error_messages = {
            "first_name": {
                "required": "First name is required",
                "max_length": "Name is too long",
            },
            "last_name": {
                "required": "Last name is required",
                "max_length": "Last Name is too long",
            },
            "gender": {
                "required": "Gender is required"
            },
        }

    def clean_gender(self):
        gender = self.cleaned_data.get("gender")
        if not gender:
            raise forms.ValidationError("Gender is required")
        return gender

    def save(self, commit=True):
        user = super().save(commit=False)
        user.role = "employee"
        if commit:
            user.save()
        return user


class EmployerRegistrationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["first_name"].label = "Company Name"
        self.fields["last_name"].label = "Company Address"
        self.fields["password1"].label = "Password"
        self.fields["password2"].label = CONFIRM_PASSWORD_STR

        self.fields["first_name"].widget.attrs.update({
            "placeholder":
            "Enter Company Name",
        })
        self.fields["last_name"].widget.attrs.update({
            "placeholder":
            "Enter Company Address",
        })
        self.fields["email"].widget.attrs.update({
            "placeholder": ENTER_EMAIL_STR,
        })
        self.fields["password1"].widget.attrs.update({
            "placeholder":
            ENTER_PASSWORD_STR,
        })
        self.fields["password2"].widget.attrs.update({
            "placeholder":
            CONFIRM_PASSWORD_STR,
        })

    class Meta:
        model = User
        fields = ["first_name", "last_name", "email", "password1", "password2"]
        error_messages = {
            "first_name": {
                "required": "First name is required",
                "max_length": "Name is too long",
            },
            "last_name": {
                "required": "Last name is required",
                "max_length": "Last Name is too long",
            },
        }

    def save(self, commit=True):
        user = super().save(commit=False)
        user.role = "employer"
        if commit:
            user.save()
        return user


class UserLoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(
        label="Password",
        strip=False,
        widget=forms.PasswordInput,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None
        self.fields["email"].widget.attrs.update(
            {"placeholder": ENTER_EMAIL_STR})
        self.fields["password"].widget.attrs.update(
            {"placeholder": ENTER_PASSWORD_STR})

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")

        if email and password:
            self.user = authenticate(email=email, password=password)

            if self.user is None:
                raise forms.ValidationError("User Does Not Exist.")
            if not self.user.check_password(password):
                raise forms.ValidationError("Password Does not Match.")
            if not self.user.is_active:
                raise forms.ValidationError("User is not Active.")

        return super().clean(*args, **kwargs)

    def get_user(self):
        return self.user


class EmployeeProfileUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["first_name"].widget.attrs.update({
            "placeholder":
            "Enter First Name",
        })
        self.fields["last_name"].widget.attrs.update({
            "placeholder":
            "Enter Last Name",
        })

    class Meta:
        model = User
        fields = ["first_name", "last_name", "gender"]
