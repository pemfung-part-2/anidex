#!/bin/bash

python3 manage.py migrate
gunicorn jobs.wsgi --bind 0.0.0.0:8000
