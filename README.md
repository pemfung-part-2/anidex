## Django Job Portal
Nev flavour of https://github.com/manjurulhoque/django-job-portal/

#### An open source online job portal.

<p align="center">
    <img alt="forks" src="https://img.shields.io/github/forks/manjurulhoque/django-job-portal?label=Forks&style=social"/>
    <img alt="stars" src="https://img.shields.io/github/stars/manjurulhoque/django-job-portal?style=social"/>
    <img alt="watchers" src="https://img.shields.io/github/watchers/manjurulhoque/django-job-portal?style=social"/>
    <img alt="github Actions" src="https://github.com/manjurulhoque/django-job-portal/workflows/job-portal/badge.svg"/>
</p>

Live: [Demo](https://pemfung-2-django-job-portal.herokuapp.com/)

Used Tech Stack:
- Django
- PostgreSQL
- Docker

## Features Screenshots

### Homepage and Browse Jobs
<img src="screenshots/one.png" height="800">

### Add New Position as employer
<img src="screenshots/two.png" height="800">

### Job Details and Apply Jobs as employee
<img src="screenshots/three.png" height="800">

## Programmer's Guide

### Project Structure

    .
    ├── ...
    ├── accounts                
    │   ├── api
    │   ├── fixtures
    │   ├── migrations
    │   ├── tests
    │   │   ├── test_forms.py
    │   │   ├── test_models.py
    │   │   ├── test_views.py
    │   │   └── ...
    │   └── ...
    ├── functional_tests                
    │   ├── base.py
    │   ├── test_login.py
    │   ├── test_register_employee.py
    │   ├── test_register_employer.py
    │   └── ...
    ├── jobs                
    │   ├── settings.py
    │   ├── urls.py
    │   ├── wsgi.py
    │   └── ...
    ├── jobsapp                
    │   ├── api
    │   ├── migrations
    │   ├── templatetags
    │   ├── tests
    │   │   ├── test_forms.py
    │   │   ├── test_models.py
    │   │   ├── test_views.py
    │   │   └── ...
    │   ├── views
    │   └── ...
    ├── tags               
    │   ├── migrations
    │   ├── tests
    │   │   ├── test_models.py
    │   │   └── ...
    │   └── ...
    └── ...

> Use short lowercase names (snake_case) for the top-level files and folders except LICENSE, README.md
### Setup application

- Create a virtual environment
  ```
  virtualenv venv
  ```
  Or
  ```
  python3 -m venv venv
  ```

- Activate virtual environment
  ```
  source venv/bin/activate
  ```

- Install the required packages in the virtual env:
  ```
  pip install -r requirements.txt
  ```

- Setup environment variable.
   - Create `.env` file from `.env.dev.sample`
     ```
     cp .env.dev.sample .env
     ```
   - Match the db settings to the settings on local db
   - Add Github client ID and client secret in the `.env` file

#### Run application

- With the venv activated, execute:
  ```
  python3 manage.py collectstatic
  ```
  *Note* : Collect static is not necessary when debug is True (in dev mode)

- Migrate the database:
  ```
  python3 manage.py migrate
  ```

- Load demo data (optional):
  ```
  python3 manage.py loaddata <app_name>/fixtures/<app_name>_initial_data.json --app app.<model_name>
  ```
  Example:
  ```
  python3 manage.py loaddata accounts/fixtures/accounts_initial_data.json --app app.User
  ```

- Run server:
  ```
  python3 manage.py runserver
  ```

#### Run with docker

- Compose containers and start application:
  ```
  docker-compose up
  ```

- Add `-d` flag to run in detached mode
  ```
  docker compose up -d
  ```

- Add `--build` flag to rebuild the application
  ```
  docker-compose up --build
  ```

- Shows running containers:
  ```
  docker ps
  ```

- Stop and remove containers:
  ```
  docker-compose down
  ```

#### Run tests:
```
python3 manage.py test
```

#### Run mutation tests:
```
python3 manage.py muttest <app_name> --modules <app_name>.views <app_name>.tests
```
Example:
```
python3 manage.py muttest accounts --modules accounts.views accounts.tests
```

#### Run load test:
```
locust --host=https://pemfung-2-django-job-portal.herokuapp.com/ --headless -u 10 -r 5 -t30s --only-summary --csv=output
```

#### Dump data:
```
python3 manage.py dumpdata --format=json --indent 4 <app_name> > <app_name>/fixtures/<app_name>_initial_data.json
```
Example:
```
python3 manage.py dumpdata --format=json --indent 4 accounts > accounts/fixtures/accounts_initial_data.json
```

#### Documentation

- [Database Structure](https://dbdiagram.io/d/5fca6d859a6c525a03b9d670)
- [API Documentation](https://djangojobportal.docs.apiary.io)


## Sonarqube Analysis
URL: https://pmpl.cs.ui.ac.id/sonarqube/dashboard?id=ppl2020-a-feb_biro_pendidikan    
Username: ppla62020    
Password: pPLUKAN    

Note: Currently we are reusing sonarqube server for ppl2020-a-feb_biro_pendidikan since we got no server,
even though the sonarqube title is for `PPL2020 A6 - Biro Pendidikan Online FEB UI`, but the content generated is
owned by this `Django Job Portal`


## Contributors

1. [@ahmad_fauzan458](https://gitlab.com/ahmad_fauzan458): Ahmad Fauzan Amirul Isnain - 1706979152
2. [@bungamaku](https://gitlab.com/bungamaku): Bunga Amalia Kurniawati - 1706022104
3. [@ardho](https://gitlab.com/ardho): Yusuf Tri Ardho Mulyawan - 1706074985
