from locust import HttpUser, between, task

class MinimumLoadTest(HttpUser):
    wait_time = between(5, 15)

    @task(1)
    def test_load_home(self):
        self.client.get("en/")

    @task(2)
    def test_load_jobs(self):
        self.client.get("en/jobs/")
