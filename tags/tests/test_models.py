from django.test import TestCase
from django.db.utils import DataError

from tags.models import Tag


class TestTagModel(TestCase):
    def setUp(self) -> None:
        self.valid_tag = {
            "name": "Alexandro",
        }
        self.tag = Tag.objects.create(**self.valid_tag)

    def test_string_length_more_than_100(self):
        invalid_tag = {
            "name": "this_is_wrong"*10
        }
        with self.assertRaises(DataError):
            Tag.objects.create(**invalid_tag)

    def test_string_with_valid_length(self):
        tag = Tag.objects.create(**self.valid_tag)
        tag.full_clean()

    def test_string_representation(self):
        self.assertEqual(str(self.tag), self.tag.name)
